<?php

/**
 * This script allows to trandsfert Chamilo courses to Moodle
 *
 * @package    core
 * @subpackage cli
 * @copyright  2017 Université de Pau Mathieu Domingo / university of Geneva
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/*
 * Import des cours de Chamilo (1.9.6) vers Moodle (3.3)
 *
 * Auteur : Mathieu Domingo
 * 
 *Coté moodle  (pour les scorms uniquement):
 * Il faut la version modifiée de /mod/scorm/lib.php que j'ai faite pour pouvoir utiliser du createfilefrompathname
 * Il faut la version modifiée de /mod/scorm/datamodels/scormlib.php pour throw une error au lieu du die dans la fonction parse(
 * Il faut la version modifiée de /course/modlib.php pour catch l'exception et faire le rollback si il y a un echec lors de l'import d'un scorm
 * Il faut crée une nouvelle catégorie caché qui va contenir tous les cours de Chamilo, recuperer l'id obtenu pour cette catégorie puis remplir la variable $categoryid avec l'id
 *
 *
 * Coté Chamilo :
 *
 * Il faut le fichier /main/newscorm/lp_controller_unsecure.php (obtenu suite à une modification du fichier /main/newscorm/lp_controller.php)
 * Il faut la version modifiée du fichier /main/newscorm/learnpath.class.php (ajout d'une fonction qui est appelée dans lp_controller_unsecure.php
 */
//pour envoyer le fichier en local vers le serveur puis l'executer
// clear; scp /home/mathieu/Bureaux/Bureau_Moodle/moodle-git-test/import_Chamilo_to_moodle.php root@moodle-test:/opt/tools/ ; md_moodle-test 'sudo -u www-data /usr/bin/php /opt/tools/import_Chamilo_to_moodle.php'
// copie des fichiers modifiés mis dans un script :
// ./copie_fichiers_modif_vers_moodle_test.sh ; md_moodle-test 'sudo -u www-data /usr/bin/php /opt/tools/import_Chamilo_to_moodle.php'

// EDIT UNIGE 2019
// author : Camille Tardy


define('CLI_SCRIPT', 1);

require_once ('config.php');
require_once ($CFG->libdir . '/clilib.php');
require_once ($CFG->dirroot . '/course/lib.php');
require_once ($CFG->dirroot . '/course/modlib.php');
require_once ($CFG->dirroot . '/lib/filestorage/file_storage.php');
require_once ('import_ChamiloMDL_lib.php');


//Variable globale : Informations de connexion a Chamilo
//!! Il faut que le serveur de moodle soit autoriser a se connecter a la bdd Chamilo !! (via grant access)
$Chamilo_db['serveur']="XXXX";
$Chamilo_db['user']="XXXX";
$Chamilo_db['pass']="XXX";
$Chamilo_db['base']="XXXX";

$Chamilo_dbo = new mysqli($Chamilo_db['serveur'] , $Chamilo_db['user'] , $Chamilo_db['pass'] , $Chamilo_db['base']);
if ($Chamilo_dbo->connect_error) {
    die('Erreur de connexion : ' . $Chamilo_dbo->connect_error."\n");
}

//path to chamilo data on moodle server (to change to the coreect value according to your set up)
$chamilo_data_path = "/moodle_data/importchamilo/";


/**
 * Function to transfert the course document folder from Chamilo server to Moodle server
 * @param $coursecode string coursecodename in chamilo
 */
function documents_transfert($coursecode){
    global $chamilo_data_path;

    $chamilo_serv="https://mon_chamilo.com";

    echo "On Copie le dossier du cours du serveur Chamilo vers Moodle \n";
    //call to transfert folder save logs to log file wget
    exec("mkdir ".$chamilo_data_path.$coursecode);
    exec("wget --no-check-certificate -nv -o ".$chamilo_data_path."logs_wget/".$coursecode.".log -4 -r -k -nH -np --cut-dirs=3 -e robots=off --reject \"*.bak\",\"index.html*\",\"frames.css*\",\"*.log.html\",\"*.log.html.tmp\"   ".$chamilo_serv."/courses/".$coursecode."/document/  -P".$chamilo_data_path.$coursecode."/");
    exec("chown -R www-data:www-data ".$chamilo_data_path.$coursecode);
    // echo "Delete images folder \n";
   // exec("rm -r ".$chamilo_data_path.$coursecode.\"/images/");
    echo "Copie des fichiers terminée \n";

}


// PAS testé ni utilisé par l'UNIGE 
// TODO: faire une vraie fonction qui permet de generer les zips des scorms de chamilo. En l'état, assez honteux comme solution de contournement..
// Dans /var/www/chamilo/main/newscorm/ j'ai modifié le fichier learnpath.class.php pour crée les zip des scorms dans le dossier scorm.
//j'ai également crée un fichier lp_controller_unsecure.php à partir du fichier lp_controller.php pour ne pas avoir besoin d'etre connecté pour executer une fonction perso qui appelle le fichier learnpath.class.php
//Appeller directement le lp_controller_unsecure.php n'est pas suffisant... cela crée bien des zips, mais il n'y a que les fichiers manifest, pas le contenu .. il faut obligatoirement etre connecté pour que cela fonctionne
//Solution alternative pour ne pas perdre de temps à faire croire que le script est connecté : se connecté dans firefox a chamilo, puis faire le "telechargement" du zip à la main
//cette fonction va generer les commandes linux qui vont bien pour ouvrir tous les onglets dans firefox. Il faut stocker le resultat de sortie de cette fonction dans un fichier script et lancé le script (en local ca ouvre firefox)
/**
 * Fonction pour créer un ZIP des parcours Chamilo
 */
function creer_zips()
{
    global $Chamilo_dbo;
    //Requete pour obtenir tous les code cours et les lp_id (des scorms de type Dokeos)
    $query='SELECT c.code, clp.id as lp_id FROM c_lp  clp, course c WHERE content_maker="Dokeos" and clp.c_id=c.id';
    if($result=$Chamilo_dbo->query($query))
    {
        //echo "Nombre de liens : ".$result->num_rows."\n";
        //Solution pourrie mais rapide : Rajout pour faire un script
        echo "#!/bin/bash \n";
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while($row=$result->fetch_object())
        {



            //print_r($row);
          //TODO edit
            $adresse='https://webcampus.univ-pau.fr/main/newscorm/lp_controller_unsecure.php?cidReq='.$row->code.'&id_session=0&gidReq=0&action=export_perso&lp_id='.$row->lp_id;
            //echo "Adresse : ".$adresse."\n";
            //echo file_get_contents($adresse);
            //readfile($adresse);
            //sleep(10);

            //Solution pourrie mais rapide : Rajout pour faire un script
            echo 'firefox "view-source:'.$adresse.'"'."\n";
            echo "sleep 2 \n";

        }

        //on "ferme" la requete
        $result->close();
    }
}




/**
 * $Function pour recupérer la liste des fichiers
 * modification pour ne pas avoir l'arborescence complete et pour separer le sous path et le nom du fichier
 * @param $dir
 * @param string $subdir
 * @return array
 */
function find_all_files($dir,$subdir='/')
{

    $defaultFilesArray = array('ListeningComprehension.mp3', 'ArtefactsInRMI.swf', 'PonderationOfMrSignal.swf', 'SpinEchoSequence.swf', 'example.flv',
        'painting.mpg', 'index.html');
    $defaultFilesGalleryArray = array('board.jpg', 'book_highlight.jpg', 'book.jpg', 'bookcase.jpg', 'computer.jpg', 'emot_happy.jpg', 'emot_neutral.jpg',
        'emot_sad.jpg', 'emot_wink.jpg', 'female.jpg', 'geometry.jpg', 'homework.jpg', 'idea.jpg', 'interaction.jpg', 'logo_dokeos.png', 'male.jpg',
        'maths.jpg', 'mechanism.jpg', 'mouse.jpg', 'newspaper.jpg', 'note.jpg', 'pencil.png', 'presentation.jpg', 'redlight.jpg', 'science.jpg', 'servicesgather.png',
        'silhouette.png', 'speech.jpg', 'time.jpg', 'tutorial.jpg', 'twopeople.png', 'world.jpg', 'write.jpg');


    $result=array(); //init array
    $root = scandir($dir.$subdir);
    foreach($root as $value)
    {
        //print_r($dir.$subdir.$value."\n"); //pour voir toute l'arborescence
        if($value === '.' || $value === '..') {continue;}

        //Il y a des "fichiers par défaut" dans certains cours -> ajout de ces fichiers a exclure des imports
        //Je ne sais pas vraiment si c'est le cas pour tous les Chamilos, mais bon dans le doute j'ajoute la modification..
        if(in_array($value,$defaultFilesArray) || ($subdir === "gallery" && in_array($value, $defaultFilesGalleryArray))) {continue;}

        if(is_file($dir.$subdir.$value)) {array_push($result, array("Path"=>$subdir,"File"=>$value));continue;}
        foreach(find_all_files($dir, $subdir.$value."/") as $value)
        {
            $result[]=$value;
        }
    }
    return $result;
}


/**
 * Function to know if a folder is empty
 * pour le "copyright" , fonction trouvée sur internet : php.net/manual/fr/function.is-dir.php#85961
 * @param $dir
 * @return bool
 */
function isEmptyDir($dir){
    return (($files = @scandir($dir)) && count($files) <= 2);
}



/**
 * Function  pour é Zip pour le dossier scorm des cours de Chamilo
 * pour le "copyright", fonction d'origine trouvée sur internet :  https://stackoverflow.com/questions/33052644/save-zip-file-in-a-different-directory-than-the-level-at-which-code-file-is-pres#question
 *
 * @param $source
 * @param $destination
 * @param bool $include_dir
 * @return bool
 */
function Zip($source, $destination, $include_dir = false)
{

    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    if (file_exists($destination)) {
        unlink ($destination);
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }
    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        if ($include_dir) {
            $arr = explode("/",$source);
            $maindir = $arr[count($arr)- 1];

            $source = "";
            for ($i=0; $i < count($arr) - 1; $i++) {
                $source .= '/' . $arr[$i];
            }

            $source = substr($source, 1);

            $zip->addEmptyDir($maindir);

        }

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}


//Fin des fonctions trouvées sur internet



//Debut des fonctions globalement OK

/**
 *  Fonction pour récupérer le texte d'intro
 * @param $cours l'objet cours en traitement
 *
 */
function get_intro_text($cours)
{
    global $Chamilo_dbo;

    $query = "SELECT intro_text  FROM c_tool_intro WHERE c_id = '". $cours->id_chamilo ."' AND id LIKE 'course_homepage'";

    //on execute la requete et on voit si on a des resultats
    if($result=$Chamilo_dbo->query($query))
    {
        $default_intro = '<p style="text-align: center;">
                        <img src="/home/main/img/mascot.png" alt="Mr. Chamilo" title="Mr. Chamilo" />
                        <h2>Bienvenue dans ce cours</h2>
                     </p>';

        $default_intro_dokeos = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tbody><tr><td width="110" valign="middle" align="left"><img src="/home/main/img/mr_dokeos.png" alt="mr. Dokeos" title="mr. Dokeos"></td><td valign="middle" align="left">Bienvenue dans cet espace de cours</td></tr></tbody></table>';

        $defaultMiddle_intro_dokeos = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="110" valign="middle" align="left"><img src="/home/main/img/mr_dokeos.png" alt="mr. Dokeos" title="mr. Dokeos" /></td><td valign="middle" align="left">Bienvenue dans cet espace de cours</td></tr></table>';

        $row=$result->fetch_object();
        //on transforme en utf8 et on stocke le resultat
        $row_intro = $row->intro_text;

        //si le texte d'intro est celui par default, ne rien faire.
        if($row_intro == $default_intro || $row_intro == $default_intro_dokeos || $row_intro == $defaultMiddle_intro_dokeos){
            //intro is default text
            $cours->intro="";
            echo "Pas d'intro pour ce cours \n";
        }else{
            //on enlève les tags HTML
            //$intro = strip_tags($row->intro_text);

            //on strip le logo dokeos et chamilo :
        /*    $mrdokeos_img = '<img src="/home/main/img/mr_dokeos.png" alt="mr. Dokeos" title="mr. Dokeos">';
            $mrdokeos_img2 = '<img title="mr. Dokeos" alt="mr. Dokeos" src="/home/main/img/mr_dokeos.png">';
            $mrchamilo_img = '<img alt="Mr. Chamilo" title="Mr. Chamilo" src="/home/main/img/mascot.png">';

            str_replace($mrdokeos_img, '', $row_intro);
            str_replace($mrdokeos_img2, '', $row_intro);
            str_replace($mrchamilo_img, '', $row_intro); */

            $cours->intro=$row_intro;
        }


        //on "ferme" la requete
        $result->close();
    }

}


/**
 *  Fonction pour récupérer le texte de descritpion
 * @param $cours l'objet cours en traitement
 * @return array of descriptions object (1 object per section "title" & "content" ordered from top to bottom)
 */
function get_cours_description($cours){
    global $Chamilo_dbo;
    $result_desc = array();

    //on ajoute au début le text d'intro qui est stocké dans une autre table
    $query_intro = "SELECT intro_text  FROM c_tool_intro WHERE c_id = '". $cours->id_chamilo ."' AND id LIKE 'course_description'";

    //on execute la requete et on voit si on a des resultats
    if($result_intro=$Chamilo_dbo->query($query_intro))
    {
        while($row=$result_intro->fetch_object()){
        /*    foreach ($row as $key => $case){
                $row->$key=utf8_encode($case);
	}*/
            $result_desc[] = $row;
        }

        //on "ferme" la requete
        $result_intro->close();
    }

    $query = "SELECT title, content  FROM c_course_description WHERE c_id = '". $cours->id_chamilo."'";

    //on execute la requete et on voit si on a des resultats
    if($result=$Chamilo_dbo->query($query))
    {
        while($row=$result->fetch_object()){
          /*  foreach ($row as $key => $case){
                $row->$key=utf8_encode($case);
	}*/
            $result_desc[] = $row;
        }

        //on "ferme" la requete
        $result->close();
    }


    return $result_desc;

}



/**
 * Fonction qui crée un cours dans une categorie et y ajoute un enseignant
 * @param int categoryid  correspond à l'id de la catégorie (1290 par exemple)
 * @param stdobject $cours est un objet qui contient les informations du cours : il faut fournir fullname shortname et idnumber (numero d'identification, pas necessairement un vrai numero)
 * A ce stade $cours doit avoir pour attribut code, directory et title et teachers_id
 **/
function creer_cours($cours)
{
    global $DB;

    //find the matching catgory id for Moodle given Chamilo.
    $categoryid = mdl_matching_category($cours->category_id);

    if($categoryid == 0){
        throw new exception('Le Cours vient d\'une catégorie archive de Chamilo : '.$cours->category_id);
    }

    $catcontext = context_coursecat::instance($categoryid);
    $editoroptions['context'] = $catcontext;
    $editoroptions['subdirs'] = 0;

    //On crée l'objet requis par la fonction create_course
    $data = new stdClass();
    $data->fullname=$cours->title;
    $data->shortname=$cours->code.'_script';
    $data->category="".$categoryid."";
    $data->summary_editor=[];
    $data->summary_editor["text"]="";//"Résumé du cours";
    $data->summary_editor["format"]="1";
    $data->format="topics";
    $data->numsections="3";
    $data->visible = false;

    //Creation du cours et "enrolement" de l'enseignement
    echo "Creation du cours ".$data->shortname.", ".$data->fullname." sur Moodle --  dans la categorie : " .$categoryid." \n"; // echo pour permettre de voir dans les logs où l'on est (les logs sont assez pauvres, il faudra ameliorer ce point si j'ai le temps :s)

   // $DB->set_debug(1);
    $course = create_course($data, $editoroptions);
    //A priori Il n'y a besoin que de l'id du coup je le stock dans l'objet cours pour les prochaines fonctions.
    $cours->id=$course->id;


    echo " * Commence le traitement de l'introduction\n";
    //UNIGE ajoute texte intro in section 0 du cours Moodle
    get_intro_text($cours);
    if(!empty($cours->intro)){
        //get section 0 of current course
        $section0 = $DB->get_record('course_sections', array('course' => $cours->id,'section' => 0), '*', MUST_EXIST);
        course_update_section($cours, $section0, array('summary' => $cours->intro));
    }
    echo "Traitement de l'intro terminé\n";


    echo " * Commence la gestion des inscriptions\n";
    //Maintenant que le cours est crée il faut inscrire l'enseignant au cours.
    //En créeant le cours on a obtenu l'id du cours, mais on a besoin du enrolid (id du cours - 1 et fois 4 mais on fait une requete pour l'obtenir proprement).
    $instance_manual = $DB->get_record('enrol', array('courseid'=>$cours->id, 'enrol'=>'manual'), '*', MUST_EXIST);
    $instance_self = $DB->get_record('enrol', array('courseid'=>$cours->id, 'enrol'=>'self'), '*', MUST_EXIST);

    echo "Inscritpion des profs\n";
    $roleid=3; //Je met le role 3 qui correspond a editing teacher, il faudra verifier en prod que c'est bien le meme numero, et si c'est bien ce role que je dois attribuer. ok 3 pour UNIGE aussi
    if (!$enrol_manual = enrol_get_plugin('manual')) {
      throw new coding_exception('Can not instantiate enrol_manual');
    }
    if(isset($cours->teachers_id)){
        foreach ($cours->teachers_id as $userid) {
            $enrol_manual->enrol_user($instance_manual, $userid, $roleid);//, $timestart, $timeend); pas besoin de preciser de date de debut et de fin des droits..
        }
    }else{
        echo "Aucun prof à ajouter au cours\n";
    }



    //UNIGE ajoute le code d'accès au cours si il existe dans Chamilo ici
    if(!empty($cours->registration_code)){
        echo "Ajoute un MDP pour l'acces au cours\n";
        $enrol_self = enrol_get_plugin('self');
        //prepare data to update in enrol instance (expirynotify needed)
        $enrol_data = new stdClass();
        $enrol_data->password = $cours->registration_code;
        $enrol_data->expirynotify = $instance_self->expirynotify;
        //update self enrol instance with MDP
        $enrol_self->update_instance($instance_self, $enrol_data);
    }else{
        echo "Pas de MDP pour ce cours\n";
    }

    echo "Gestion des inscriptions terminée \n";
    return $course;
}


/**
 * Function to query Chamilo DB to retrieve all questions for a given course
 * returns the results.
 * @param $cours
 * @return $questions array of questions objects
 */
function get_questions($cours)
{
    global $Chamilo_dbo;

    $query_question = "select qq.id, qq.question, qq.description, qq.ponderation, qq.type from c_quiz_question qq  where qq.c_id = '".$cours->id_chamilo."'";

    $questions_pre = array();
    $questions = array();

    //on execute la requete et on stock les resultats dans results
    if ($result = $Chamilo_dbo->query($query_question)) {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while ($row = $result->fetch_object()) {
           /* foreach ($row as $key => $case) {
                $row->$key = utf8_encode($case);
	}*/
            $questions_pre[] = $row;
        }

        // on trie les questions qu'on importe pas (celles par default)
        foreach($questions_pre as $q){
            if ($q->question == 'L\'ironie socratique consiste à...' || $q->question == 'L\'ironia socratica consiste nel...' || $q->question == 'Socratic irony is...' || $q->question == 'Sokraattinen ironia on...') {
                //on supprime la question par default
                continue;
            }else{
                //on traite les quizs pour chaque question que l'on ajoute comme tag a la question :
                $tags = array();

                //get answers
                $query_tags = "SELECT exercice_id AS quiz_id, question_order AS q_order FROM c_quiz_rel_question q_quiz WHERE q_quiz.c_id = '".$cours->id_chamilo."' AND question_id ='".$q->id."'";

                if ($result_tag = $Chamilo_dbo->query($query_tags)) {
                    //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
                    while ($row = $result_tag->fetch_object()) {
                      /*  foreach ($row as $key => $case) {
                            $row->$key = utf8_encode($case);
		    }*/
                        $tags[] = $row;
                    }
                }

                // On enregistre les tags dans l'objet question
                $q->tags = $tags;

                //on "ferme" la requete des tags
                $result_tag->close();



                //on traite les réponses de chaque question
                $answers = array();

                //get answers
                $query_answer = "select a.id, a.answer, a.correct, a.comment, a.ponderation from c_quiz_answer a  where a.c_id = '".$cours->id_chamilo."' and a.question_id = '".$q->id."'";

                if ($result_a = $Chamilo_dbo->query($query_answer)) {
                    //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
                    while ($row = $result_a->fetch_object()) {
                       /* foreach ($row as $key => $case) {
                            $row->$key = utf8_encode($case);
		    }*/
                        $answers[$row->id] = $row;
                    }
                }
                // On enregistre la question avec ses reponses
                $q->answers = $answers;

                //on "ferme" la requete des answers
                $result_a->close();
            }
            //on enregistre la qusetion dan sle tableau des qiuestions du quiz.
            $questions[] = $q;
        }

        echo "Nombre de questions à importer : " . count($questions) . "\n"; //pour les logs
        //on "ferme" la requete des questions
        $result->close();

    }

    return $questions;
}


/**
 * Fucntion to retreive the questions catgeories for a given course in Chamilo
 *
 * @param $cours
 * @return array $questions categories empty if no categ
 */
function get_questions_category($cours){
    global $Chamilo_dbo;

    $q_categ = array();
    $cat = array();

    $query_cat = "select cat.id, cat.title from c_quiz_question_category cat  where cat.c_id = '".$cours->id_chamilo."'";

    //on execute la requete et on stock les resultats dans results
    if ($result = $Chamilo_dbo->query($query_cat)) {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while ($row = $result->fetch_object()) {
           /* foreach ($row as $key => $case) {
                $row->$key = utf8_encode($case);
	}*/
            $q_categ[$row->id] = $row;
        }
    }
    //on "ferme" la requete des questions
    $result->close();


    //if no category skip this part
    if(!empty($q_categ)){
        $query_qcat = "select qcat.question_id, qcat.category_id from c_quiz_question_rel_category qcat  where qcat.c_id = '".$cours->id_chamilo."'";
        //on execute la requete et on stock les resultats dans results
        if ($result_qcat = $Chamilo_dbo->query($query_qcat)) {
            //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
            while ($row = $result_qcat->fetch_object()) {
              /*  foreach ($row as $key => $case) {
                    $row->$key = utf8_encode($case);
	    }*/
                $cat[] = $row;
            }
        }
        //on "ferme" la requete des questions
        $result_qcat->close();

        //on ajoute les questions id au categories
        foreach($cat as $c){
            $q_categ[$c->category_id]->questions[]=$c->question_id;
        }
    }

    return $q_categ;
}

/**
 * function to retreive the list qof quizs from a course
 *
 * @param $cours
 * @return $quizs array
 */
function get_quizs_list($cours){
    global $Chamilo_dbo;

    $quizs = array();

    $query_quiz = "SELECT c_quiz.id, c_quiz.title, c_quiz.description, c_quiz.active FROM c_quiz WHERE  `c_id` = '".$cours->id_chamilo."'";
    //on execute la requete et on stock les resultats dans results
    if ($result = $Chamilo_dbo->query($query_quiz)) {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while ($row = $result->fetch_object()) {
           /* foreach ($row as $key => $case) {
                $row->$key = utf8_encode($case);
	}*/
            $quizs[] = $row;
        }
    }

    //on "ferme" la requete des questions
    $result->close();

    return $quizs;
}


/**
 * Fucntion to retreive the questions catgeories for a given course in Chamilo
 *
 * @param $cours
 * @return array $q_gloss glossary entries empty if no entries
 */
function get_glossary_entries($cours){
    global $Chamilo_dbo;

    $gloss = array();

    $query_gloss = "SELECT g.name, g.description FROM c_glossary as g WHERE g.c_id = '".$cours->id_chamilo."'";

    //on execute la requete et on stock les resultats dans results
    if ($result = $Chamilo_dbo->query($query_gloss)) {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while ($row = $result->fetch_object()) {
           /* foreach ($row as $key => $case) {
                $row->$key = utf8_encode($case);
	}*/
            $gloss[] = $row;
        }
    }
    //on "ferme" la requete
    $result->close();


    return $gloss;
}


/**
 * function to retrieve the list of surveys from a course
 *
 * @param $cours
 * @return $feedbacks array
 */
function get_feedback_list($cours){
    global $Chamilo_dbo;

    $feedbacks = array();

    $query_feedbk = "SELECT s.survey_id as id, s.title, s.subtitle, s.intro, s.surveythanks as thanks  FROM c_survey as s WHERE s.c_id = '".$cours->id_chamilo."'";
    //on execute la requete et on stock les resultats dans results
    if ($result = $Chamilo_dbo->query($query_feedbk)) {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while ($row = $result->fetch_object()) {
           /* foreach ($row as $key => $case) {
                $row->$key = utf8_encode($case);
	}*/
            $feedbacks[] = $row;
        }
    }

    //on "ferme" la requete des questions
    $result->close();

    return $feedbacks;
}

/**
 * function to retrieve the list of survey questions from a course
 *
 * @param $feedback
 * @param $cours
 * @return $questions array
 */
function get_feedback_questions($feedback, $cours){
    global $Chamilo_dbo;

    $feedback_questions = array();

    $query_feedbk_quest = "SELECT q.question_id as id, q.survey_question as question, q.survey_question_comment as comment, q.type, q.display, q.sort, q.max_value FROM c_survey_question as q 
where q.c_id = '".$cours->id_chamilo."' AND survey_id = '".$feedback->id."'";
    //on execute la requete et on stock les resultats dans results
    if ($result = $Chamilo_dbo->query($query_feedbk_quest)) {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while ($row = $result->fetch_row()) {
           /* foreach ($row as $key => $case) {
                $row->$key = utf8_encode($case);
	}*/
            $feedback_questions[] = $case;
        }
    }

    $result->close();

    // on traite les options des questions
    $type_options = array("yesno", "multiplechoice", "multipleresponse", "dropdown", "score");

    foreach($feedback_questions as $fbk_q){
        if(in_array($fbk_q->type, $type_options)){
            // on doit récupérer les options pour ces types de questions
            $options = array();

            $query_fbk_quest_option = "SELECT option_text as txt FROM c_survey_question_option
where c_id = '".$cours->id_chamilo."' AND question_id = '".$fbk_q->id."'";
            //on execute la requete et on stock les resultats dans results
            if ($result_opt = $Chamilo_dbo->query($query_fbk_quest_option)) {
                while ($row = $result_opt->fetch_object()) {
                  /*  foreach ($row as $key => $case) {
                        $row->$key = utf8_encode($case);
		}*/
                    $options[] = $row;
                }
            }

            $result_opt->close();

            //on ajoute les options à l'objet question
            $fbk_q->options = $options;
        }
    }

    return $feedback_questions;
}


/**
 * Fonction qui recupere les liens de chamilo et les retourne dans un tableau
 *  @param $cours  l'objet cours en traitement
 */
function get_links($cours){
    global $Chamilo_dbo;

    //Etape 1 : obtenir les liens et les titles a partir du cours via une requete sur la table c_link de Chamilo
   // $query="select l.url, l.title, l.description from c_link l , course c where c.id=l.c_id and code='".$cours->code."'";
    $query="SELECT l.url, l.title, l.description, p.lastedit_type, p.visibility from c_link l , course c, c_item_property p where c.id=l.c_id AND p.c_id =c.id and code='".$cours->code."' ANd p.ref = l.id AND p.tool LIKE 'link' AND p.lastedit_type not like 'LinkDeleted' AND p.visibility < 2";

    $links_pre = array();
    $links = array();

    //on execute la requete et on stock les resultats dans results
    if($result=$Chamilo_dbo->query($query))
    {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while($row=$result->fetch_object()) {
            //bien que la base de données soit indiqué avec un encodage en utf8, cela n'a pas l'air d'etre le cas des donnéees qui sont ?|  l'interieur ...
            //du coup on boucle pour convertir chaque string en ut8 avant de les mettre dans le tableau retourné
           /* foreach ($row as $key => $case) {
                $row->$key=utf8_encode($case);
	}*/
            $links_pre[]= $row;
        }

        //On supprime Google et Wikipedia de la liste des liens
        foreach($links_pre as $link){
            if ($link->title == 'Google' || $link->title == 'Wikipedia') {
                //on supprime google et Wikipedia
                continue;
            }else{
                $links[] = $link;
            }
        }
        echo "Nombre de liens à importer : ". count($links) ."\n"; //pour les logs
        //on "ferme" la requete
        $result->close();
    }

    $cours->links=$links;
}




/**
 * Fonction qui recupere les identifiants (moodle) des enseignants d'un cours.
 * Premier traitement recupere les username du coté Chamilo puis les utilises pour obtenir les ids du coté Moodle
 * A la fin de la fonction la variable $cours contient un attribut teachers_id ($cours->teachers_id) qui contient un tableau des ids.
 */
function get_teachers_id($cours)
{
    global $Chamilo_dbo, $DB, $chamilo_data_path;

    //Etape 1 : il va falloir trouver l'id moodle des enseignants du cours pour pouvoir les enroler
    //Etape1_1 : Obtenir l'username des enseignants du coté Chamilo
    $query='SELECT shibb_unique_id, username, email '
            . 'FROM course_rel_user cu, user u '
            . 'WHERE u.user_id=cu.user_id and cu.status=1 and cu.course_code="'.$cours->code.'"';

    $chamilo_users = array();

    //on execute la requete et on voit si on a des resultats
    if($result=$Chamilo_dbo->query($query))
    {
        echo "Nombre d'enseignant dans le cours : ".$result->num_rows."\n"; //Pour les logs
        if($result->num_rows==0) {
            throw new exception("Il n'y a pas d'enseignant dans le cours");
        }

        //On initialise plutot à 0 et on evite de gerer l'apparition du OR qui devient systematique
        $select='0';
        while($row=$result->fetch_row()) {
            $select=$select.' OR username="'.$row[0].'"';
            $chamilo_users[] = $row[0];
        }

        //Etape1_2 : Obtenir l'id du coté moodle grace à l'username
        $userids = $DB->get_records_sql('SELECT id, username FROM {user} where '.$select);

        $teachersid=[];
        foreach ($userids as $userid) {
            $teachersid[]=$userid->id;
            $teachersusernameMDL[]=$userid->username;
        }

        $missing_teachers = array();
        $missing_teachers = array_diff($chamilo_users, $teachersusernameMDL);

        if(!empty($missing_teachers)){
            echo "Tous les enseignants du cours ne sont pas dans le DB Moodle\n";
            $text_log = "****** Cours : ".$cours->code." -- ".$cours->title."\n";
            foreach($missing_teachers as $mt){
                $text_log .= $mt." \n";
            }
            file_put_contents($chamilo_data_path.'missingTeachersMDL.txt', $text_log ,FILE_APPEND);
        }

        //On stock le resultat
        $cours->teachers_id=$teachersid;

        //on "ferme" la requete
        $result->close();
    }
}


/**
 * Fonction qui recupere la liste des documents du cours. Save it in $cours->docs object
 *  @param $cours  l'objet cours en traitement
 */
function get_documents_list($cours){
    global $Chamilo_dbo;

    //Etape 1 : obtenir les liens et les titles a partir du cours via une requete sur la table c_link de Chamilo
    $query=" SELECT d.path, d.comment, d.title, i.visibility, i.lastedit_type as visible
            FROM course c, c_item_property i
            INNER JOIN c_document d 
            ON ( d.id = i.ref AND i.c_id = d.c_id ) 
            WHERE i.tool =  'document'
            AND c.id = d.c_id
            AND c.id ='".$cours->id_chamilo."'
            AND d.path NOT LIKE  '/images%'
            AND d.path NOT LIKE  '%/chat_files%' ";

   /* $query="select d.path, d.comment, d.title from c_document d , course c where c.id=d.c_id and code='".$cours->code."' AND path NOT LIKE '/images%' AND path NOT LIKE '%/chat_files%'"; */

    $docs = array();

    //on execute la requete et on stock les resultats dans results
    if($result=$Chamilo_dbo->query($query))
    {
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while($row=$result->fetch_object()) {
            //bien que la base de données soit indiqué avec un encodage en utf8, cela n'a pas l'air d'etre le cas des donnéees qui sont ?|  l'interieur ...
            //du coup on boucle pour convertir chaque string en ut8 avant de les mettre dans le tableau retourné
          /*  foreach ($row as $key => $case) {
                $row->$key=utf8_encode($case);
            }*/
            $docs[]= $row;
        }

        echo "Nombre de documents avant triage dans le cours : ". count($docs) ."\n"; //pour les logs
        //on "ferme" la requete
        $result->close();
    }

    $cours->docs=$docs;
}





/**
 * Fonction qui retourne code directory et title pour l'ensemble des cours de Chamilo. Il suffit de modifier la requete pour traiter seulement une partie des cours.
 *
 */
function get_Chamilo_cours($where)
{
    //TODO : rajouter un parametre optionnelle qui rajoute une condition au select si le parametre est non vide.
    global $Chamilo_dbo;
    $return=[];

	if(empty($where)){
	$where= "c.code='TESTCOURS'";
	}

    //requete pour obtenir des informations de base sur un cours
    //$query="Select code, directory, title, id, registration_code from course where ".$where; //limite à un cours pour les tests
    $query="SELECT c.code, c.directory, c.title, c.id, c.registration_code, c.category_code, cat.id as category_id 
                FROM course c, course_category cat
                WHERE cat.code = c.category_code
                AND ".$where;
    //$query="Select code, directory, title, id, registration_code from course where YEAR( last_visit ) >2014 order by code";

    //on execute la requete et on stock les resultats dans results
    if($result=$Chamilo_dbo->query($query))
    {
        echo "Nombre de cours à transferer : ".$result->num_rows."\n"; //Pour les logs
        //tant qu'il y a des lignes on crée un objet que l'on push dans le tableau que l'on retourne
        while($row=$result->fetch_object())
        {
            //bien que la base de données soit indiqué avec un encodage en utf8, cela n'a pas l'air d'etre le cas des données qui sont à l'interieur ...
            //du coup on boucle pour convertir chaque string en ut8 avant de les mettre dans le tableau retourné
          /* foreach ($row as $key => $case) {
                $row->$key=utf8_encode($case);
	}*/
            $return[]= $row;
	}

        //on "ferme" la requete
        $result->close();
    }

    return $return;
}


/**
 * Fonction qui crée le fichier descritpion dans le cours
 * @param $course
 * @param $result_descriptions
 * @throws dml_exception
 * @throws moodle_exception
 */
function import_description($course,$description){
    global $DB;

    $content="";
    $intro = "";

    if(!empty($description[0]->intro_text)) {
        $intro = $description[0]->intro_text;
        $content .= "<p>".$intro."</p>";
    }

    foreach($description as $desc){
        $content .= "<h3>".$desc->title."</h3>";
        $content .= "<p>".$desc->content."</p>";
    }

    //On commence par crée le "module" (fichier pour commencer, dossier un peu plus loin) qui va contenir le fichier dans le cours
    $modulename="page";//Pour ajouter une page.
    $section=0; //On ajoute la description dans la section 0 du cours avec l'introduction et le forum annonce

    $moduleinfo = new stdClass();
    $moduleinfo->modulename = $modulename;
    $module = $DB->get_record('modules', array('name'=>$modulename), '*', MUST_EXIST);
    $moduleinfo->module = $module->id;
    $moduleinfo->section = $section; // This is the section number in the course. Not the section id in the database.
    $moduleinfo->course = $course->id;
    $moduleinfo->visible = true;
    $moduleinfo->display = 5;//type d'affichage : automatique, nouvelle fenetre, inclus dans moodle, dans notre cas : open pour ouvrir le lien lors du clique : RESOURCELIB_DISPLAY_OPEN
    $moduleinfo->printintro = 0;// on n'affiche pas l'intro dans le cours
    $moduleinfo->printheading =1;
    $moduleinfo->contentformat = 1;
    $moduleinfo->introformat = 1;
    $moduleinfo->name = "Description du cours";
    $moduleinfo->content = $content;
    $moduleinfo->format = "topics";

    $moduleinfo = add_moduleinfo($moduleinfo, $course, null);

    echo "La page de description à été créée. \n";
}

/**
 * Function to create a ressource (file) in moodle
 * @param $section
 * @param $course
 * @param $document
 * @param $folderpath
 * @param $doc_name
 * @param $doc_comment
 * @param $userid
 * @throws dml_exception
 * @throws file_exception
 * @throws moodle_exception
 * @throws stored_file_creation_exception
 */
function create_resource($section, $course, $document, $folderpath, $doc_name, $doc_comment, $userid, $indent, $visible){

    global $DB;

    $modulename = "resource";//Pour ajouter un fichier
    $moduleinfo = new stdClass();
    $moduleinfo->modulename = $modulename;
    $module = $DB->get_record('modules', array('name' => $modulename), '*', MUST_EXIST);
    $moduleinfo->module = $module->id;
    $moduleinfo->section = $section; // This is the section number in the course. Not the section id in the database.
    $moduleinfo->introformat = 1;
    $moduleinfo->course = $course->id;
    $moduleinfo->visible = $visible;
    $moduleinfo->name = $doc_name;
    $moduleinfo->files = null;
    $moduleinfo->intro = $doc_comment;
    $moduleinfo->introformat = 1;
    $moduleinfo->printintro = 1;
    $moduleinfo->showsize = 1;
    $moduleinfo->showdate = 1;
    $moduleinfo->showdescription = 1;
    $moduleinfo->display = 0;
    $moduleinfo = add_moduleinfo($moduleinfo, $course, null);

    //Le module est crée, il peut contenir le fichier que l'on ajoute donc maintenant
    $context = context_module::instance($moduleinfo->coursemodule);
    $filerecord = new \stdClass();
    $filerecord->contextid = $context->id;
    $filerecord->component = "mod_" . $modulename;
    $filerecord->filearea = "content";
    $filerecord->itemid = 0;
    $filerecord->sortorder = 0;
    $filerecord->filepath = "/"; //"/";
    $filerecord->filename = $document;
    $filerecord->userid = $userid;

    //si on est dans un dossier on indente le fichier pour faire propre sur la page du cours
    if($indent){
        $cm = get_coursemodule_from_instance($modulename, $moduleinfo->instance, $moduleinfo->course);
        $cm->indent += 1;
        $DB->set_field('course_modules', 'indent', $cm->indent, array('id'=>$cm->id));

        rebuild_course_cache($cm->course);
    }

    //on verifie que le fichier n'existe pas avant de l'enregistrer
    $fs = get_file_storage();
    if ($fs->file_exists($filerecord->contextid, $filerecord->component, $filerecord->filearea, $filerecord->itemid, $filerecord->filepath, $filerecord->filename)) {
        echo "le fichier " . $folderpath.$document . " existe deja\n"; //pour les logs (peu probable mais bon..)
    } else {
        //On enregistre le fichier
        $stored_file = $fs->create_file_from_pathname($filerecord, $folderpath.$document);
        //var_dump($stored_file);
    }


}

/**
 * Fonction pour importer les documents en tant que ressources et non pas comme dossiers.
 *
 * @param $folderpath
 * @param $course
 * @param $cours
 * @throws dml_exception
 * @throws file_exception
 * @throws moodle_exception
 * @throws stored_file_creation_exception
 */
function import_documents_libre($folderpath,$course,$cours,$section,$forceimport)
{
    global $DB;
    $userid = $cours->teachers_id[0];

    // $section = 1; //organisation choisi empiriquement à la creation du cours: section 1 pour les documents section 2 pour les liens sections 3 pour les scorms

    echo "#### Import LIBRE des documents \n ####";

    //On verifie que le dossiers documents existe bien
    if (!(file_exists($folderpath) && is_readable($folderpath))) { //documents n'existe pas
        echo "le fichier " . $folderpath . " n'existe pas ou n'est pas lisible\n"; //pour les logs
    } else //documents existe
    {
        echo "On importe les documents inclus dans " . $folderpath . "\n"; //pour les logs

        //DONE : Amelioration : au lieu de faire un seul dossier documents qui contient tous les fichiers et documents, on regarde ce qu'il y a dans le dossier documents et on differencie dossier et fichier seul :
        //Si on a un fichier on ajoute le fichier seul
        //Si on a un dossier, on l'ajoute (avec tous ses fichiers et sous dossiers à l'interieur)

        $documents = scandir($folderpath);
        foreach ($documents as $document) {
            //on ne traite pas . et ..
            if ($document === '.' || $document === '..' || $document === ".thumbs" || $document === 'certificates' || $document === 'Groupe_groupdocs') {
                continue;
            }
            if (containDeleted($document)) {continue;}
            if(isGroupdocs($document)){continue;}

            $search = "/" . $document;
           // $doc_name = utf8_encode($document);
	   $doc_name= $document;
	    $doc_comment = "";

            $filefound = false;
            $visible = 1;

            for ($i = 0; $i < count($cours->docs); $i++) {
                if ($cours->docs[$i]->path == $search) {
                  //  $doc_name = utf8_encode($cours->docs[$i]->title);
		//  $doc_comment = utf8_encode($cours->docs[$i]->comment);
		    $doc_name  = $cours->docs[$i]->title;
		    $doc_comment = $cours->docs[$i]->comment;
                    $filefound = true;
                    if(isset($cours->docs[$i]->visibility)){
                        $visible = $cours->docs[$i]->visibility ;
                    }
                    break;
                }
            }

            // if file not in array do not import
            if (!$filefound && !$forceimport) {
                echo "Le dossier " . $folderpath . $document . " est un dossier supprimé on ne l'importe pas \n"; //pour les logs
                continue;
            }

            //Si l'on a un fichier on ajoute un module de type fichier et le fichier
            if (is_file($folderpath . "/" . $document)) {
                create_resource($section, $course, $document, $folderpath, $doc_name, $doc_comment, $userid, false, $visible);
            }

            //Si on a un repertoire et qu'il n'est pas vide, on importe le repetoire (avec fichiers et sous dossier)
            if (is_dir($folderpath . "/" . $document) and !isEmptyDir($folderpath . "/" . $document)) {
                //on evite d'importer les conversations ...
                //Ne pas importer le dossier "Historique de conversation dans le chat" et ses sous dossiers
                if ($document === "chat_files" || $document === "css" || $document === "images") {
                    echo "On n'importe pas le dossier : " . $folderpath . $document . "\n"; //pour les logs
                    continue;
                }

                //On verifie ici que le dossier contient bien des documents, si il n'en contient pas on evite de crée un dossier vide :)
                $files = find_all_files($folderpath . "/" . $document, "/");
                if (empty($files)) {
                    echo "Le dossier " . $folderpath . $document . " ne contient pas de fichiers \n"; //pour les logs
                    continue; //on ne crée pas le dossier et on passe au dossier suivant
                }

                //on créer un label avec le nom du répertoire
                $modulename = "label";//Pour ajouter un label
                $moduleinfo = new stdClass();
                $moduleinfo->modulename = $modulename;
                $module = $DB->get_record('modules', array('name' => $modulename), '*', MUST_EXIST);
                $moduleinfo->module = $module->id;
                $moduleinfo->section = $section; // This is the section number in the course. Not the section id in the database.
                $moduleinfo->introformat = 1;
                $moduleinfo->course = $course->id;
                $moduleinfo->visible = $visible;
                $moduleinfo->name = $doc_name;
                $moduleinfo->intro = "<h5 style='padding-top: 20px;'><i class='fa fa-folder-open'></i> ".$doc_name."</h5>";
                $moduleinfo->introformat = 1;
                $moduleinfo = add_moduleinfo($moduleinfo, $course, null);

                foreach ($files as $file) {
                    $pathfile=$file["Path"];
                    $filename=$file["File"];
                    $pathname=$folderpath."/".$document.$pathfile.$filename;

                    if(containDeleted($pathname)){continue;}
                    if(isGroupdocs($document)){continue;}

                    $search="/".$document.$pathfile.$filename;
                    $doc_name=$filename;
                    $subfilefound = false;
                    $visible = 1;

                    for($i = 0; $i < count($cours->docs); $i++) {
                        if($cours->docs[$i]->path == $search) {
                            //$doc_name  = utf8_encode($cours->docs[$i]->title);
                            //$doc_comment = utf8_encode($cours->docs[$i]->comment);
				$doc_name  = $cours->docs[$i]->title;
				$doc_comment = $cours->docs[$i]->comment;
				$subfilefound = true;
                            if(isset($cours->docs[$i]->visibility)){
                                $visible = $cours->docs[$i]->visibility ;
                            }
                            break;
                        }
                    }

                    // if file not in array do not import
                    if(!$subfilefound){
                        echo "Le sous fichier ".$folderpath.$document.$pathfile.$filename." est un fichier supprimé on ne l'importe pas \n"; //pour les logs
                        continue;
                    }else{
                        //on crée la ressource file
                        create_resource($section, $course, $filename, $folderpath."/".$document.$pathfile , $doc_name, $doc_comment, $userid, true, $visible);
                    }


                }

            }
        }

    }

}



/**
 * Fonction qui importe des documents dans moodle
 * @param string folderpath correspond au chemin du repertoire qui contient les differents fichiers et dossier à importer (le repertoire en question n'est pas importé, seulement son contenu)
 * @param stdobject course correspond à l'objet retournée apres la creation du cours ou s'obtient grace à l'id du cours avec $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
 * @param stdobject data  est un objet qui contient les informations du cours : il faut fournir fullname shortname et idnumber (numero d'identification, pas necessairement un vrai numero)
 * a voir pour $data si je trouve un "résumé" du cours dans Chamilo ou non.
 * @return stdobject course est un objet que l'on retourne qui permet d'avoir l'id du cours que l'on vient de créer
 **/
function import_documents($folderpath,$course,$cours){
    global $DB;
    $userid = $cours->teachers_id[0];

    $section=1; //organisation choisi empiriquement à la creation du cours: section 1 pour les documents section 2 pour les liens sections 3 pour les scorms

    echo "#### Import DOSSIER des documents \n ####";

    //On verifie que le dossiers documents existe bien
    if (!(file_exists($folderpath) && is_readable($folderpath))) { //documents n'existe pas
      echo "le fichier ".$folderpath." n'existe pas ou n'est pas lisible\n"; //pour les logs
    }
    else //documents existe
    {
        echo "On importe les documents inclus dans ".$folderpath."\n"; //pour les logs

    //DONE : Amelioration : au lieu de faire un seul dossier documents qui contient tous les fichiers et documents, on regarde ce qu'il y a dans le dossier documents et on differencie dossier et fichier seul :
    //Si on a un fichier on ajoute le fichier seul
    //Si on a un dossier, on l'ajoute (avec tous ses fichiers et sous dossiers à l'interieur)

        $documents= scandir($folderpath);
        foreach ($documents as $document) {
            //on ne traite pas . et ..
            if($document === '.' || $document === '..' || $document===".thumbs" || $document === 'certificates' || $document === 'Groupe_groupdocs'){continue;}
            if(containDeleted($document)){continue;} //{echo $document." contient deleted \n";continue;}
            if(isGroupdocs($document)){continue;}

            $search="/".$document;
	    // $doc_name=utf8_encode($document);
	    $doc_name=$document;
            $doc_comment="";

            $filefound=false;
            $visible = 1;

            for($i = 0; $i < count($cours->docs); $i++) {
                if($cours->docs[$i]->path == $search) {
                    //$doc_name  = utf8_encode($cours->docs[$i]->title);
                    //$doc_comment = utf8_encode($cours->docs[$i]->comment);
		    $doc_name  = $cours->docs[$i]->title;
		    $doc_comment = $cours->docs[$i]->comment;
		    $filefound = true;
                    if(isset($cours->docs[$i]->visibility)){
                        $visible = $cours->docs[$i]->visibility ;
                    }
                    break;
                }
            }

            // if file not in array do not import
            if(!$filefound){
                echo "Le dossier ".$folderpath.$document." est un dossier supprimé on ne l'importe pas \n"; //pour les logs
                continue;
            }

            //Si l'on a un fichier on ajoute un module de type fichier et le fichier
            if(is_file($folderpath."/".$document))
            {
                create_resource($section, $course, $document, $folderpath, $doc_name, $doc_comment, $userid, false, $visible);
            }

            //Si on a un repertoire et qu'il n'est pas vide, on importe le repetoire (avec fichiers et sous dossier)
            if(is_dir($folderpath."/".$document) and !isEmptyDir($folderpath."/".$document))
            {
                //on evite d'importer les conversations ...
                //Ne pas importer le dossier "Historique de conversation dans le chat" et ses sous dossiers
                if($document==="chat_files" || $document==="css" || $document==="images")
                {
                    echo "On n'importe pas le dossier : ".$folderpath.$document."\n"; //pour les logs
                    continue;
                }

                //On verifie ici que le dossier contient bien des documents, si il n'en contient pas on evite de crée un dossier vide :)
                $files=find_all_files($folderpath."/".$document, "/");
                if(empty($files))
                {
                    echo "Le dossier ".$folderpath.$document." ne contient pas de fichiers \n"; //pour les logs
                    continue; //on ne crée pas le dossier et on passe au dossier suivant
                }


                //echo $folderpath."/".$document." est un dossier, on ajoute un module dossier et on l'y met dedans avec tout son contenu\n";
                //Une fois que l'on sait que le fichier existe bien et est lisible, on commence par crée le "module" (dossier cette fois ci) qui va contenir le fichier dans le cours
                $modulename="folder";//folder pour ajouter un dossier.
                $moduleinfo = new stdClass();
                $moduleinfo->modulename = $modulename;
                $module= $DB->get_record('modules', array('name'=>$modulename), '*', MUST_EXIST);
                $moduleinfo->module=$module->id;
                $moduleinfo->section = $section; // This is the section number in the course. Not the section id in the database.
                $moduleinfo->course = $course->id;
                $moduleinfo->visible = $visible;
                $moduleinfo->name=$doc_name; // Ce name correspond au nom du dossier qui va contenir les fichier que l'on met en dedans.
                $moduleinfo->display=0; //Il y a une option dans la table folder pour savoir si l'on affiche le dossier dans le cours ou sur une nouvelle page. 1 pour afficher dans le cours, 0 pour afficher dans une nouvelle page
                $moduleinfo->intro=$doc_comment;
		$moduleinfo->format="topics";
		$moduleinfo->files=null;
                $moduleinfo->introformat=1;
                $moduleinfo->printintro=1;
                $moduleinfo->showexpanded=0;//Empeche d'afficher le contenu des sous dossiers
                $moduleinfo->showdownloadfolder=1; //Option qui permet d'afficher un bouton pour telecharger le dossier(1) ou non (0)
                $moduleinfo = add_moduleinfo($moduleinfo, $course, null);

                $context = context_module::instance($moduleinfo->coursemodule);

                foreach ($files as $file) {

                    $pathfile=$file["Path"];
                    $filename=$file["File"];
                    $file_extension = pathinfo($filename, PATHINFO_EXTENSION);
                    $pathname=$folderpath."/".$document.$pathfile.$filename;

                    if(containDeleted($pathname)){continue;}
                    if(isGroupdocs($document)){continue;}

                    $search="/".$document.$pathfile.$filename;
                    $doc_name=$filename;
                    $subfilefound = false;

                    for($i = 0; $i < count($cours->docs); $i++) {
                        if($cours->docs[$i]->path == $search) {
                            //$doc_name_sansext  = utf8_encode($cours->docs[$i]->title);
			    $doc_name_sansext  = $cours->docs[$i]->title;
			    $doc_name = $doc_name_sansext.'.'.$file_extension;
                            // $doc_comment = utf8_encode($cours->docs[$i]->comment);
                            $subfilefound = true;
                            break;
                        }
                    }

                    // if file not in array do not import
                    if(!$subfilefound){
                        echo "Le sous fichier ".$folderpath.$document." est un fichier supprimé on ne l'importe pas \n"; //pour les logs
                        continue;
                    }

                    $filerecord = new stdClass();
                    $filerecord->contextid=$context->id;
                    $filerecord->component="mod_".$modulename;
                    $filerecord->filearea="content";
                    $filerecord->itemid=0;
                    $filerecord->sortorder=0;
                    $filerecord->filepath=$pathfile; //"/";
                    $filerecord->filename=$doc_name;
                    $filerecord->userid=$userid;

                    //on verifie que le fichier n'existe pas avant de l'enregistrer
                    $fs = get_file_storage();
                    if($fs->file_exists($filerecord->contextid, $filerecord->component, $filerecord->filearea,$filerecord->itemid, $filerecord->filepath, $filerecord->filename))
                    {
                      echo "le fichier ".$pathname." existe deja \n"; //pour les logs (mais peu probable)
                    }
                    else
                    {
                      //echo "On enregistre le fichier : ".$pathname."\n"; //pour les logs (flood)
                      $stored_file=$fs->create_file_from_pathname($filerecord,$pathname);
                    }
                }

            }
        }
    }
}



/**
 * Function qui importe les liens dans le cours moodle
 * @param $course cours moodle object
 * @param $url liens url
 * @param $urlname nom de l'url
 * @param $desc descritpion de l'url
 */
function import_liens($course, $url, $urlname, $desc, $visibility){
    global $DB;

    //On commence par crée le "module" (fichier pour commencer, dossier un peu plus loin) qui va contenir le fichier dans le cours
    $modulename="url";//Pour ajouter un lien.
    $section=2; //organisation choisi empiriquement à la creation du cours: section 1 pour les documents section 2 pour les liens sections 3 pour les scorms

    $moduleinfo = new stdClass();
    $moduleinfo->modulename = $modulename;
    $module = $DB->get_record('modules', array('name'=>$modulename), '*', MUST_EST);
    $moduleinfo->module = $module->id;
    $moduleinfo->section = $section; // This is the section number in the course. Not the section id in the database.
    $moduleinfo->course = $course->id;
    $moduleinfo->visible = $visibility;
    $moduleinfo->display = 5;//type d'affichage : automatique (0), nouvelle fenetre, inclus dans moodle, dans notre cas : open (5) pour ouvrir le lien lors du clique : (RESOURCELIB_DISPLAY_OPEN) _AUTO pour automatique
    $moduleinfo->showdescription = 1;// on affiche pas la description dans le cours
    $moduleinfo->introformat = 1;
    $moduleinfo->externalurl = $url;
    $moduleinfo->name = $urlname;
    $moduleinfo->intro = $desc;
    $moduleinfo->format = "topics";

    $moduleinfo = add_moduleinfo($moduleinfo, $course, null);
}


/**
 * Function qui importe les glossaires dans le cours moodle
 * @param $course cours moodle object
 * @param $glossaries le tableaux des glossaires
 */
function import_glossary($course, $gloss_entries){
    global $DB;

    //On commence par crée le "module" (fichier pour commencer, dossier un peu plus loin) qui va contenir le fichier dans le cours
    $modulename="glossary";//Pour ajouter un lien.
    $section=3; //organisation choisi empiriquement à la creation du cours: section 1 pour les documents section 2 pour les liens sections 3 pour les scorms

    $moduleinfo = new stdClass();
    $moduleinfo->modulename = $modulename;
    $module = $DB->get_record('modules', array('name'=>$modulename), '*', MUST_EXIST);
    $moduleinfo->module = $module->id;
    $moduleinfo->section = $section; // This is the section number in the course. Not the section id in the database.
    $moduleinfo->course = $course->id;
    $moduleinfo->visible = true;
    $moduleinfo->showdescription = 0;// on affiche pas la description dans le cours
    $moduleinfo->introformat = 1;
    $moduleinfo->displayformat = "dictionary";
    $moduleinfo->name = "Glossaire";
    $moduleinfo->intro = "";
    $moduleinfo->defaultapproval = 1;
    $moduleinfo->assessed = 0;
    $moduleinfo->cmidnumber = "";
    $moduleinfo->format = "topics";

    $moduleinfo = add_moduleinfo($moduleinfo, $course, null);

    $context = context_module::instance($moduleinfo->coursemodule);

    //add entires to glossary
    foreach($gloss_entries as $ge){
        $def = array();
        $def['text'] = $ge->description;
        $def['format'] = 1;

        $entry = new \stdClass();
        $entry->id = "";
        $entry->concept = $ge->name;
        $entry->definition_editor = $def;
        $entry->aliases = "";

        $entry = glossary_edit_entry($entry, $course, $module, $moduleinfo, $context);
    }


}



/**
 * Fonction qui importe les surveys dans le cours moodle
 * @param $course cours moodle object
 * @param $cours cours chamilo object
 * @param $feedbacks le tableaux des feedbacks
 */
function import_feedbacks($course, $cours, $feedbacks){
    global $DB;

    //On commence par crée le "module" (fichier pour commencer, dossier un peu plus loin) qui va contenir le fichier dans le cours
    $modulename="feedback";//Pour ajouter un feedback.
    $section=3; //organisation choisi empiriquement à la creation du cours: section 1 pour les documents section 2 pour les liens sections 3 pour les scorms

    foreach($feedbacks as $fbk){
        $after_submit = array();
        $after_submit['text'] = $fbk->thanks;
        $after_submit['format'] = 1;
        $after_submit['itemid'] = "";

        $moduleinfo = new stdClass();
        $moduleinfo->modulename = $modulename;
        $module = $DB->get_record('modules', array('name'=>$modulename), '*', MUST_EXIST);
        $moduleinfo->module = $module->id;
        $moduleinfo->section = $section; // This is the section number in the course. Not the section id in the database.
        $moduleinfo->course = $course->id;
        $moduleinfo->visible = true;
        $moduleinfo->showdescription = 0;// on affiche pas la description dans le cours
        $moduleinfo->introformat = 1;
        $moduleinfo->name = $fbk->title;
        $moduleinfo->intro = $fbk->subtitle."<br/><br/>".$fbk->intro;
        $moduleinfo->page_after_submit = $fbk->thanks;
        $moduleinfo->page_after_submit_format = 1;
        $moduleinfo->page_after_submit_editor = $after_submit;
	$moduleinfo->format = "topics";


        $moduleinfo = add_moduleinfo($moduleinfo, $course, null);

        //add feedback questions via xml import only
        create_feedback_questions_xml($cours, $fbk);
    }

}


/**
 * fonction qui ajoute des scorms dans un cours
 *
 * @param $scormpath
 * @param $course
 * @param $userid par default le premier enseignant du cours
 * @throws dml_exception
 */
function import_scorms($scormpath,$course,$userid){
    global $DB,$nb_erreur;

    $section=3 ; //organisation choisi empiriquement à la creation du cours: section 1 pour les documents section 2 pour les liens sections 3 pour les scorms

    if(!is_dir($scormpath))
    {
        echo "Le dossier scorm n'existe pas dans ce cours\n";
    }
    else
    {
        echo "On importe les scorms inclus dans".$scormpath." \n";
        //echo "le dossier scorm existe dans ce cours\n";
        //le dossier existe on verifie si il contient des trucs
        $scormfiles=scandir($scormpath);

        foreach ($scormfiles as $scormfile) {
            //on ne traite pas
            if($scormfile=="." or $scormfile==".."){continue;}

            $scormdirectory=$scormpath."/".$scormfile;
            //si c'est un fichier, on verifie que c'est un zip (crée préalablement par la fonction moche "creer_zips" pour gerer les Scorms de type Dokeos qui ne sont pas dans le dossier scorm, contrairement a ceux de type Scorm et Chamilo qui sont deja présent(peu clair, voir "content_maker" de la table "c_lp" dans chamilo pour voir les differents type de scorms).
            if(!is_dir($scormdirectory))
            {
                $info= new SplFileInfo($scormdirectory);

                if($info->getExtension()!="zip")
                {
                    continue;
                }
                else
                {
                    $zipreturn=true; //on a bien un zip
                    $scormfile=$info->getBasename('.zip'); //pour avoir le nom du fichier sans l'extension .zip
                    $pathzip=$scormpath."/";
                }
            }
            else //si on a un dossier, on en crée un zip à coté du script
            {
                //echo $scormdirectory." est un dossier\n";
                //$destinationlocalzip="./".$scormfile.".zip";//on zip les dossiers dans le dossier du script
                $destinationlocalzip= __DIR__ . "import_Chamilo_to_Moodle.php/" .$scormfile.".zip";//on zip les dossiers dans le dossier du script
                $zipreturn=Zip($scormdirectory."/", $destinationlocalzip);
                $pathzip= __DIR__ . "import_Chamilo_to_Moodle.php/";
            }

            if($zipreturn)
            {

                //echo "Zip existant : ".$scormfile." , on ajoute maintenant le scorm dans le cours\n"; //pour les logs (flood/doublon)

                //Module SCORM :

                //Une fois que l'on sait que le fichier existe bien et est lisible, on commence par crée le "module" (dossier cette fois ci) qui va contenir le scorm dans le cours
                $modulename="scorm";

                $moduleinfo = new stdClass();
                $moduleinfo->modulename = $modulename;
                $module= $DB->get_record('modules', array('name'=>$modulename), '*', MUST_EXIST);
                $moduleinfo->module=$module->id;
                $moduleinfo->section = $section;
                $moduleinfo->course = $course->id;
                $moduleinfo->visible = true;
                $moduleinfo->name=$scormfile; //nom du scorm qui apparait dans le cours
                $moduleinfo->scormtype="local";
                $moduleinfo->maxattempt=0; //nombre max de tentative : infini
                $moduleinfo->intro=""; //Il n'y a pas l'air d'y avoir de texte d'intro du coté Chamilo à mettre ici, mais champs obligatoire
                $moduleinfo->width = 100;
                $moduleinfo->height = 500;
                $moduleinfo->packagename=$scormfile.".zip";
                $moduleinfo->packagepath=$pathzip;//la fonction zip enregistre les fichiers a coté du script, on les recupere donc directement ici
                $moduleinfo->userid=$userid;
		$moduleinfo->format = "topics";

                try{
                $moduleinfo = add_moduleinfo($moduleinfo, $course, null); //va dans /mod/scorm/lib.php que j'ai modifié (et qui fait un echo pour les logs) (doublon avec le echo au dessus)
                }catch(Exception $ex)
                {
                    echo "!! Erreur : Echec lors de l'import de ".$scormfile."\n\n"; //pour les logs
                    $nb_erreur+=1;
                }
                unlink($destinationlocalzip); // on supprime le fichier si l'on vient de le crée à coté du zip
            }
        }
    }
}



/**
 * Function pour connaitre les fichier/dossiers supprimé
 * dans chamilo les documents ne sont pas reelement supprimé, ils sont renommé en suffixant _DELETED suivi d'un numero
 *
 * @param $file
 * @return false|int
 */
function containDeleted($file)
{
    $pattern="#_DELETED_[0-9]+#";
    return preg_match($pattern, $file);
}


/**
 * Function pour identifier les dossiers de groupes
 *
 * @param $file
 * @return false|int
 */
function isGroupdocs($file)
{
    $pattern = "/.*_groupdocs/";
    return preg_match($pattern, $file);
}


/**
 * Main fucntion that runs the script
 *
 * La variable $cours est construite comme ci :
 * stdClass Object(
[code] => CHAMILO2
[directory] => CHAMILO2
[title] => Atelier Chamilo 2
[id] => 2439
[registration_code] =>
[id_chamilo] => 5676
[teachers_id] => Array
    ([0] => 12391
    [1] => 22)
[intro] => ....
[intro] => array of docs )

 *
 * @param $cli_option string option returned by the cli : Chamilo code for the course to upload (1 at a time)
 * @param $iscoursecode bool is option a course code or the select for course selection ?
 * @param $isin if parma cli is "in" (a list of course codes.)
 * @param $doclibre bool should we import the docs in folders or all as resources?
 * @throws dml_exception
 * @throws moodle_exception
 */
function main($cli_option, $iscoursecode, $isin, $doclibre){

    $scripttimestart = microtime(true); //pour les logs
    echo "[INFO] Debut du script import_Chamilo_to_moodle \nDate du jour : ".date(DATE_RFC850)."\n"; //pour les logs
    // fake-plugin
    $string['pluginname'] = "Import Chamilo To Moodle";

    global $DB, $nb_erreur, $chamilo_data_path;

    $nb_erreur=0;

    //Informations necessaires pour la creation du cours

    //Dans moodle, j'ai crée une categorie Chamilo expres pour contenir tous les cours de Chamilo
    //TODO UNIGE adapter la catégorie pour chaque cours. Pour les ranger correctement dès le départ.
    //UNIGE MOODLE 2018 categ ID = 201
   // $categoryid=201;

    //On recupere l'ensemble des cours : le code, le directory (correspond au dossier qui contient les données) et le title

    $where ="";
    if($iscoursecode){
        $where = "c.code='".$cli_option."'";
    }else if($isin){
        $str = $cli_option;
        $str = preg_replace('/,/', "','", $str);
        $where = "c.code IN ('".$str."')";
    }/*else{
        $where = $cli_option;
    }*/

    $tab_cours=get_Chamilo_cours($where);
$nbcours = sizeof($tab_cours);
    //On parcours tous les cours (si il y a besoin de faire un traitement seulement sur certains cours il faudra reduire la selection des cours dans get_Chamilo_cours())
    foreach ($tab_cours as $key => $cours) {
	//on donne un format par défaut au cours pour éviter les erreurs de variable non définie plus tard (à la suppression des sections)
	$cours->format = "topics";

        //$cours->code $cours->directory et $cours->title
        echo "\nTraitement du cours ".$cours->code.", #".$key."/".$nbcours."\n";

	echo "cours titre : ".$cours->title."\n";
        //on sauvegarde l'id du cours Chamilo originel
        $cours->id_chamilo = $cours->id;

        try{
            echo "# On  récupère les profs \n";
            get_teachers_id($cours); //Doit remplir la variable teachers_id du cours (et doit etre un array)
        }catch(Exception $e) {
            $nb_erreur+=1;
            echo "!! Erreur : ".$cours->code." ".$e->getMessage()."\n\n"; //Pour les logs
            continue;//on passe au cours suivant
        }

        if(count($cours->teachers_id)==0) {
            //echo $cours->code."\n"; $nb_erreur+=1;
        }

        try{
            echo "# On  crée le cours \n";
            //category_id de chamilo = $cours->category_id
            $course = creer_cours($cours); // Doit remplir la variable id du cours (un int)
        }catch(Exception $e) {
            $nb_erreur+=1;
            echo "!! Erreur : ".$e->getMessage()."\n\n"; //pour les logs
            continue;//on passe au cours suivant
        }
        echo "Le nouveau cours Moodle à pour id :".$cours->id." / ID chamilo = ".$cours->id_chamilo."\n";

        try{
            echo "# On  crée la page description du cours \n";
            $desc = get_cours_description($cours);
            if(!empty($desc)){
                import_description($course,$desc);
            }else{
                echo "Pas de description pour ce cours \n";
            }

        }catch(Exception $e) {
            $nb_erreur+=1;
            echo "Erreur : ".$e->getMessage()."\n\n"; //pour les logs
            continue;//on passe au cours suivant
        }


        /******
         *  Section 1 Docs
         ******/

        echo "# On traite les documents \n";
        //maintenant que l'on a crée le cours il faut recuperer le dossier du cours Chamilo pour avoir les fichiers et les mettre dans le cours de moodle
        documents_transfert($cours->directory);

        $coursepath=$chamilo_data_path.$cours->directory."/"; //pour s'approcher de la structure que l'on aura : un chemin jusqu'au dossier du cours

        $section1 = $DB->get_record('course_sections', array('course' => $cours->id,'section' => 1), '*', MUST_EXIST);
        course_update_section($cours, $section1, array('name' => 'Documents'));
        //On attribut tous les uploads au 1er des enseignants ..

        //on récupère la liste des docs de la DB de Chamilo pour le cours en traitement
        get_documents_list($cours);

        if($doclibre || sizeof($cours->docs) < 50){
            import_documents_libre($coursepath, $course, $cours, 1, false);
        }else{
            import_documents($coursepath, $course, $cours, 1, false);
        }



        /******
         *  Section 2 Liens
         ******/
        $delete_section2 = false;

        //on recupere les lien du cours
        echo "# On ajoute les liens \n";
        get_links($cours); //rempli l'attribut $cours->links
        $section2 = $DB->get_record('course_sections', array('course' => $cours->id,'section' => 2), '*', MUST_EXIST);
        if(!empty($cours->links)){ //if Links not empty
            //Une fois que l'on a l'ensemble des liens, on les parcours et on appelle la fonction qui importe les liens
            foreach ($cours->links as $link) {
                import_liens($course, $link->url, $link->title, $link->description, $link->visibility);
            }
        }else{
            echo "Aucun liens a importer, on supprime la section \n";
            $delete_section2 = true;
        }


        /******
         *  Section 3 Import
         ******/
        $delete_section3 = false;
        //import the created doc in section 3 of the course
        $section3 = $DB->get_record('course_sections', array('course' => $cours->id,'section' => 3), '*', MUST_EXIST);

        //on recupere les glossaires du cours
        echo "# On récupère les entrées de Glossaire \n";
        $gloss_entries = get_glossary_entries($cours);
        $noGloss = false;
        if(!empty($gloss_entries)){
           // create_glossary_xml($cours, $gloss_entries);
            import_glossary($course, $gloss_entries);
        }else{
            echo "Aucun glossaire a importer \n";
            $noGloss = true;
        }


        /*
         * TODO : fix xml creation, before using
        //on recupere les surveys/feedbacks du cours
        echo "# On récupère les entrées de Feedback \n";
        //retrieve feedback and feedback question data from Chamilo DB
        $feedbacks = get_feedback_list($cours);
        if(!empty($feedbacks)){
            echo " --> ".sizeof($feedbacks)." feedback(s) a importer \n";
            foreach($feedbacks as $f) {
                $questions = get_feedback_questions($f, $cours);
                $f->questions = $questions;
            }
            //import feedbacks to moodle
            import_feedbacks($course, $cours, $feedbacks);
        }else{
            echo "Aucun feedback a importer \n";
        }
        */


        //on recupere la banque de question du cours
        echo "# On récupère la banque de question \n";
        $questions = get_questions($cours);
        $q_categories = get_questions_category($cours);
        $noQuiz = false;

        $qbankpath = $chamilo_data_path."qBanks/".$cours->directory."/";
        exec("mkdir ".$qbankpath);
        exec("chown -R www-data:www-data ".$qbankpath);

        create_questionbank_xml($cours, $questions, $q_categories);
        if(!empty($questions)){
            //only create quiz map if questions exists.
            create_quizmap_xml($cours);
        }else{
            echo "Aucune question de Quiz a importer \n";
            $noQuiz = true;
        }

        if($noQuiz && $noGloss){
            $delete_section3 = true;
        }

        import_documents_libre($qbankpath, $course, $cours, 3, true);





       /*UNIGE --> pas les scorms
         //Fonction (sale) creer_zip doit etre executer a part (une fois) avant sur le Chamilo qui a crée tuos les zip dans les dossiers scorm des cours de chamilo
        $scormpath=$coursepath."scorm";
        import_scorms($scormpath, $course, $cours->teachers_id[0]);*/


       //Rename or Delete each sections here

        if(!$delete_section3){
            echo "# On Renome la 3eme section \n";
            course_update_section($cours, $section3, array('name' => 'Fichiers d\'imports'));
        }else{
            echo "# On Delete la 3eme section \n";
            course_delete_section($cours, $section3);
        }

        if(!$delete_section2){
            echo "# On Renome la 2eme section \n";
            course_update_section($cours, $section2, array('name' => 'Liens'));
        }else{
            echo "# On Delete la 2eme section \n";
            course_delete_section($cours, $section2);
        }

       /* if(!empty($section1->sequence)){
            echo "# On Renome la 1ere section \n";
            course_update_section($cours, $section1, array('name' => 'Documents'));
        }else{
            echo "# On Delete la 1eresection \n";
            course_delete_section($cours, $section1);
        }*/



       //delete files form serrver now that they have been imported in Moodle
        echo " #### Delete temporary data in importChamilo folder\n";
        exec("rm -rf ".$chamilo_data_path.$cours->directory);
        exec("rm -rf ".$chamilo_data_path."qBanks/".$cours->directory);
        echo " -------Course import finished----------------------------------------- \n";


    }

    echo "\n\nNombre d'erreurs pour tous les cours : ".$nb_erreur."\n"; //pour les logs

    $scripttimeend = microtime(true);
    $scripttime = $scripttimeend - $scripttimestart;
    $page_load_time = number_format($scripttime, 3);
    echo "\n[INFO] Fin du script à : ".date("H:i:s", $scripttimeend)."\n"; //pour les logs
    echo "[INFO] Temps d'exécution : ".$page_load_time."s"."\n"; //pour les logs

}



// options CLI

// Define the input options.
$longparams = array(
    'help' => false,
    'libre' => false,
    'coursecode' => '',
    'select' => '',
    'in'=>''
);

$shortparams = array(
    'h' => 'help',
    'l' => 'libre',
    'c' => 'coursecode',
    's' => 'select',
    'i'=> 'in'
);

list($options, $unrecognized) = cli_get_params($longparams, $shortparams);

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    $help =
"Import des cours de Chamilo a moodle. Must use only one of the options : select or coursecode

Options:
-h, --help                      Affiche cette aide
-l, --libre                     Importer les documents en format libre sinon en version dossier
-c, --coursecode=coursecode     Code Chamilo du cours à importer
-s, --select=select             where clause (SQL) for course selection in Chamilo DB >> Select code, directory, title, id, registration_code from course where XXXXX
-i, --in=in                      where clause (SQL) for course code 'IN' in Chamilo DB >> Select code, directory, title, id, registration_code from course where code IN (XXXXX)


Example:,
\$ sudo -u www-data /usr/bin/php import_Chamilo_to_moodle.php
\$ sudo -u www-data /usr/bin/php import_Chamilo_to_moodle.php -l --coursecode=CHAMILO1
\$ sudo -u www-data /usr/bin/php import_Chamilo_to_moodle.php --select
\$ sudo -u www-data /usr/bin/php import_Chamilo_to_moodle.php --in=1111,2222,44444
";

    echo $help;
    die;
}
$isin = false;
$iscoursecode = false;
$doclibre = $options['libre'];

if ($options['coursecode'] == '' ) {
   //no course code given, try where clause
    if (!empty($options['in'])) {
        /* cli_heading('SQL WHERE clause');
         $prompt = "Enter where clause :";
         $importoption = cli_input($prompt);*/

        $importoption = $options['in'];
        $isin = true;
    }
} else {
    $importoption = $options['coursecode'];
    $iscoursecode = true;
}


main($importoption, $iscoursecode, $isin, $doclibre);

