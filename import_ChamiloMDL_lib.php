<?php

/**
 * This library allows to transfert Chamilo courses to Moodle
 * and host the function relative to the file in MDL XML creation
 *
 * @package    core
 * @subpackage cli
 * @copyright  2019 University of Geneva Camille Tardy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/*

Array CATGEORIES
(
    [1] => stdClass Object
        (
            [id] => 1
            [title] => Categorie question multi
            [questions] => Array
                (
                    [0] => 6
                    [1] => 7
                )
        )
)

QUESTIONS exple

[22] => stdClass Object
        (
            [id] => 22
            [question] => Quel était...
            [description] =>
            [ponderation] => 1.00
            [type] => 2
            [answers] => Array
                (
                    [0] => stdClass Object
                        (
                            [id] => 1
                            [answer] => <p>&nbsp;1234</p>
                            [correct] => 0
                            [comment] =>
                            [ponderation] => 0.00
                        )

                    [1] => stdClass Object
                        (
                            [id] => 2
                            [answer] => <p>&nbsp;1545</p>
                            [correct] => 0
                            [comment] =>
                            [ponderation] => 0.00
                        )

                    [2] => stdClass Object
                        (
                            [id] => 3
                            [answer] => <p>&nbsp;1665</p>
                            [correct] => 1
                            [comment] =>
                            [ponderation] => 1.00
                        )
                )
        )
 */


/**
 * @param $categ
 * @param $course
 */
function set_question_category($categ){
    $text = '<!-- question: 0  -->
            <question type="category">
                <category>
                ';

    $text .= "<text>\$course\$/top/".$categ->title."</text>";
    $text .= " 
                </category>
            </question> \n \n";

    return $text;
}


/**
 * create xml for beginning of question same for all types
 * @param $question
 * @return string
 */
function set_question_common_header($question, $qtype){
    $name = $question->question;
    if(isset($question->name)){
        $name = $question->name;
    }

    $question_text = $question->question;
    if(isset ($question->description)){
        $question_text .= "<br/>".$question->description;
    }


    $txt ="<!-- question:  -->
            <question type=\"".$qtype."\">
             <name>
                <text>".$name."</text>
            </name>
            <questiontext format=\"html\">
              <text><![CDATA[".$question_text."]]></text>
            </questiontext>
            <generalfeedback format=\"html\">
                 <text></text>
            </generalfeedback>
            <defaultgrade>".$question->ponderation."</defaultgrade>
            <penalty>0.0000000</penalty>
            <hidden>0</hidden>";

    return $txt;
}


/**
 * create xml for question type multichoice
 * @param $question question object with its answer
 * @param $uniqueanswer bool true if only one answer possible
 * @return string
 */
function set_question_multichoice($question, $uniqueanswer){
    if($uniqueanswer){
        $unique = "true";
    }else{
        $unique = "false";
    }

    $txt ="<single>".$unique."</single>
            <shuffleanswers>true</shuffleanswers>
            <answernumbering>abc</answernumbering>
            <shownumcorrect/>";

    $txt_ans = "";
    foreach($question->answers as $ans){
        $answ_txt = strip_tags($ans->answer);

        //transform ponderation in % value for moodle to guess which answers are true
        $ponderation = ($ans->ponderation/$question->ponderation)*100;
        $txt_ans .="
                    <answer fraction=\"".$ponderation."\" format=\"html\">
                      <text><![CDATA[".$answ_txt."]]></text>
                      <feedback format=\"html\">
                        <text><![CDATA[<p>".$ans->comment."</p>]]></text>
                      </feedback>
                    </answer>";
    }

    $txt .= $txt_ans;

    return $txt;
}


/**
 * create xml for question type cloze
 * @param $question
 * @return string
 */
function set_question_cloze($question){
    //get te values between [] as cloze answers. MDL --> {1:SHORTANSWER:=XXXX}
    $first_key = key($question->answers);
    $text_cloze = $question->answers[$first_key]->answer;

    $patterns = array("/\[/","/\]/");
    $replacements =  array("{1:SHORTANSWER:=", "}");
    $result_txt_cloze = preg_replace($patterns, $replacements, $text_cloze);

    //remove weird characters from Chamilo cloze
    $result_txt = preg_replace('/::(.*)@/','',$result_txt_cloze);

    $txt = "<!-- question:  -->
      <question type=\"cloze\">
        <name>
          <text>".$question->question."</text>
        </name>
        <questiontext format=\"html\">
        <text><![CDATA[".$result_txt."]]></text>
        </questiontext>
        <generalfeedback format=\"html\">
            <text><![CDATA[".$question->answers[$first_key]->comment."]]></text>
        </generalfeedback>
        <penalty>0.3333333</penalty>
        <hidden>0</hidden>
      ";

    return $txt;
}


/**
 * create xml for question type matching
 * @param $question
 * @return string
 */
function set_question_matching($question){
       $txt = " <shuffleanswers>true</shuffleanswers>
        <shownumcorrect/>";

    foreach($question->answers as $ans){
        // in chamilo : answer->correct = 0 for questions and question_id for answers
        if ($ans->correct > 0){
            //strip html tags and office tags
            $answ_txt = strip_tags($question->answers[$ans->correct]->answer);

            $txt .= " 
                    <subquestion format=\"html\">
                      <text><![CDATA[".$ans->answer."]]></text>
                      <answer>
                        <text>".$answ_txt."</text>
                      </answer>
                    </subquestion>\n";
        }

    }

    return $txt;
}


/**
 * create xml for question type essay
 * @param $question
 * @return string
 */
function set_question_essay($question){
    $txt = "
            <responseformat>editor</responseformat>
            <responserequired>1</responserequired>
            <responsefieldlines>15</responsefieldlines>
            <attachments>0</attachments>
            <attachmentsrequired>0</attachmentsrequired>
            <graderinfo format=\"html\">
            <text><![CDATA[<p></p>]]></text>
            </graderinfo>
            <responsetemplate format=\"html\">
                <text></text>
            </responsetemplate>
        ";

    return $txt;
}


/**
 * create xml for endg of question same for all types plus tags if they exist
 * @param $question
 * @return string
 */
function set_question_footer($question){
    $txt="";

   if(!empty($question->tags)){
       $txt = "<tags>
            ";
       foreach($question->tags as $tag){
           $txt .= "<tag><text>Quiz_".$tag->quiz_id."</text></tag>
            ";
           $txt .= "<tag><text>Quiz_".$tag->quiz_id."-QuestionOrder_".$tag->q_order."</text></tag>
            ";
       }
       $txt .= "</tags>
            ";
   }

    $txt .="
    </question>\n \n";

    return $txt;
}

/**
 * function that return s the xml for the question and dispatch according to the question type
 * @param $q_id
 * @param $questions
 * @return string
 */
function set_question_xml($question, $errors_qbank){
    $q_type = $question->type;
    $text_head_q = "";
    $text_q = "";
    $end_text ="";
    $result = new \stdClass;

    switch ($q_type) {
        case 1:
            if(sizeof($question->answers)<2){
                $errors_qbank = add_question_error_list($question, "A multichoice question must have at least 2 answers.", $errors_qbank);
            }elseif(all_answers_empty($question)){
                $errors_qbank = add_question_error_list($question, "A multichoice question can not have all its answers empty.", $errors_qbank);
            }else{
                // echo "Q type = multichoice 1R \n";
                $text_head_q = set_question_common_header($question,'multichoice');
                $text_q = set_question_multichoice($question, true);
                $end_text = set_question_footer($question);
            }
            break;
        case 2:
            if(sizeof($question->answers)<2){
                $errors_qbank = add_question_error_list($question, "A multichoice question must have at least 2 answers.", $errors_qbank);
            }else{
                // echo "Q type = multichoice +R \n";
                $text_head_q = set_question_common_header($question,'multichoice');
                $text_q = set_question_multichoice($question, false);
                $end_text = set_question_footer($question);
            }
            break;
        case 3:
           // echo "Q type = cloze \n";
            $text_q = set_question_cloze($question);
            $end_text = set_question_footer($question);
            break;
        case 4:
            $question = question_answertext_length($question);

            // echo "Q type = matching \n";
            $text_head_q = set_question_common_header($question, 'matching');
            $text_q = set_question_matching($question);
            $end_text = set_question_footer($question);
            break;
        case 5:
          //  echo "Q type = essay \n";
            $text_head_q = set_question_common_header($question,'essay');
            $text_q = set_question_essay($question);
            $end_text = set_question_footer($question);
            break;
    }

    $text_quest = $text_head_q.$text_q.$end_text;

    $result->text = $text_quest;
    $result->errors = $errors_qbank;

    return $result;
}


/**
 * check if all the answers of a given question are empty. returns a boolean
 * @param $question
 * @return bool
 */
function all_answers_empty($question){
    $all_empty = true;

    foreach($question->answers as $ans){
        // if at least one answer is empty its ok
       if(strlen($ans->answer)>0){
           $all_empty = false;
       }
    }

    return $all_empty;
}


/**
 * Verify if the question answer text is too long for moodle. If so write the answers in the question text and replace the answer txt by their ID.
 * @param $question
 * @return mixed
 */
function question_answertext_length($question){
    $edit_question = false;
    $answers = $question->answers;
    $qname = $question->question;

    foreach($answers as $a){
        if(($a->correct == 0) && (strlen($a->answer)>250)){
            //text answer is too big must implement it in the question text
            $edit_question = true;
            echo "** Inspect question Matching  --> Edit question Txt : answer too long ".$question->question." \n";
        }
    }

    //edit the question object now if needed answer text in question text and answer id as answer text.
    if($edit_question){
        $question_txt = " <br/><br/> ******* <br/> Réponses possibles : <br/>";
        foreach($answers as $a){
            //we only extract the answers not the questions of the answer object
            if($a->correct == 0){
                $question_txt .= "<br/>".$a->id.". ".$a->answer."<br/>";
                $a->answer = $a->id;
            }
        }
        $question->question .= $question_txt;
        $question->name = $qname;
    }

    return $question;
}


/**
 * List the questions we did not import in the question bank xml file with a short error msg.
 * @param $question
 * @param $error_txt
 * @param $errors_qbank
 */
function add_question_error_list($question, $error_txt, $errors_qbank){
    echo "****** Write to the Question error log \n";

    $error_txt = '"'.$question->question.'","'.$error_txt.'"'."\r\n";
    $errors_qbank[] = $error_txt;

    return $errors_qbank;
}


/**
 * function to write the actual log file from the array that list all errors.
 * @param $errors_qbank
 * @param $cours
 */
function write_error_log_file($errors_qbank, $cours){
    $fileLocation = "/srv/moodlenfs/importchamilo/qBanks/".$cours->directory."/";
    $pathfile = $fileLocation.$cours->code."_QuestionErrorList.csv";

    $qErrorFile = fopen($pathfile, "w");
    //Start of file
    $txt_bof = '"Question title","Error text"'."\r\n";
    fwrite($qErrorFile, $txt_bof);

    foreach($errors_qbank as $e){
        fwrite($qErrorFile, $e);
    }

    //save file
    fclose($qErrorFile);
}


/**
 * Function that creates the file (in MDL XML) to import the question bank to MDL course
 *
 * @param $cours
 * @param $course
 * @param $questions
 * @param $categories
 */
function create_questionbank_xml($cours, $questions, $categories){
    echo "****** Create the question bank file \n";

    $errors_qbank = array();

    if(!empty($questions)){
        //do not cretae qbank file if no question in Chamilo.

        $fileLocation = "/srv/moodlenfs/importchamilo/qBanks/".$cours->directory."/";
        $qBankFile = fopen($fileLocation.$cours->code."_QuestionBank.xml", "w");
        //Start of file
        $txt_bof = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                <quiz>
                ";
        fwrite($qBankFile, $txt_bof);

        $processed_q_ids=array();

        //first treat questions within categories
        foreach($categories as $cat){
            //create category in file
            $txt_cat = set_question_category($cat);
            fwrite($qBankFile, $txt_cat);
            $txt_cat = "";

            //add questions after category
            foreach($cat->questions as $key=>$q_id){
                $processed_q_ids[] = $q_id;
                $result_xml = set_question_xml($questions[$q_id],$errors_qbank);
                $errors_qbank = $result_xml->errors;
                fwrite($qBankFile, $result_xml->text);
                $result_xml = array();
            }
        }

        //now treat the rest of the questions
        foreach($questions as $key=>$q){
            //only process questions if they haven't been done yet
            if(!in_array($key, $processed_q_ids)){
                $rslt_xml = set_question_xml($q, $errors_qbank);
                $errors_qbank = $rslt_xml->errors;
                fwrite($qBankFile, $rslt_xml->text);
                $rslt_xml = array();
            }
        }

        //end of file
        $txt_eof = "</quiz>\n";
        fwrite($qBankFile, $txt_eof);
        fclose($qBankFile);

    }

    if(!empty($errors_qbank)){
        write_error_log_file($errors_qbank, $cours);
    }
}


/**
 * Function that creates the file (in MDL XML) to import the question bank to MDL course
 *
 * @param $cours
 * @param $course
 * @param $questions
 * @param $categories
 */
function create_quizmap_xml($cours){
    echo "****** Create the quiz map file \n";

    $quizs = get_quizs_list($cours);

    $fileLocation = "/srv/moodlenfs/importchamilo/qBanks/".$cours->directory."/";
    $qMapFile = fopen($fileLocation.$cours->code."_QuizMap.csv", "w");
    //Start of file
    $txt_bof = '"id","title","description","actif"'."\r\n";
    fwrite($qMapFile, $txt_bof);


    foreach($quizs as $q){
        $active = "non";
        if($q->active > 0){
            $active = "oui";
        }

        $txt = '"'.$q->id.'","'.$q->title.'","'.$q->description.'","'.$active.'"'."\r\n";
        fwrite($qMapFile, $txt);
    }

    //save file
    fclose($qMapFile);

}


/**
 * function to create the options for multiplichoice feedback question type
 * @param $question
 * @return mixed|string
 */
function set_feedback_question_multioptions($question){
    $text = "";
    //add all options

    $size = sizeof($question->options);
    foreach($question->options as $key => $opt){
        $text .= $opt;
        if($key<($size-1)){
            //last option do not add the pipe
            $text .= "
        |";
        }
    }
    //if horizontal display
    if($question->display == "horizontal"){
        $text .= "<<<<<1";
    }

    return $text;
}


/**
 * create the xml for a given feedback uestion type
 * @param $question
 * @return string
 */
function set_feedback_question_xml($question){
    //TODO : fix xml. not interpreted correctly once imported in Moodle

    $xml = "";
    $quest = new \stdClass();
    $quest->text = $question->question;

    switch ($question->type) {
        case "pagebreak" :
            $quest->type = "pagebreak";
            $quest->text = "";
            $quest->presentation = "";
            break;
        case "open":
            $quest->type = "textarea";
            $quest->presentation = "35|5";
            break;
        case "percentage":
            $quest->type = "numeric";
            $quest->presentation = "0|100";
            break;
        case "score":
      // on doit faire une question par option --> a traiter en amont
            $quest->type = "numeric";
            $quest->presentation = "0|".$question->max_value;
            break;
        case "comment":
            $quest->type = "label";
            $quest->text = "";
            $quest->presentation = $question->question;
            break;
        case "dropdown":
            //dropdown
            $quest->type = "multichoice";
            $quest->presentation = "d>>>>>";
            $quest->presentation .= set_feedback_question_multioptions($question);
            break;
        case "multipleresponse":
            //check boxes
            $quest->type = "multichoice";
            $quest->presentation = "c>>>>>";
            $quest->presentation .= set_feedback_question_multioptions($question);
            break;
        default :
            //radio bouton  (yesno and multiplechoice)
            $quest->type = "multichoice";
            $quest->presentation = "r>>>>>";
            $quest->presentation .= set_feedback_question_multioptions($question);
            break;


    }

    $xml = "
    <ITEM TYPE=\"".$quest->type."\" REQUIRED=\"0\">
               <ITEMID>
               </ITEMID>
               <ITEMTEXT>
                    <![CDATA[".$quest->text."]]>
               </ITEMTEXT>
               <ITEMLABEL>
                    <![CDATA[]]>
               </ITEMLABEL>
               <PRESENTATION>
                    <![CDATA[".$quest->presentation."]]>
               </PRESENTATION>
               <OPTIONS>
                    <![CDATA[]]>
               </OPTIONS>
               <DEPENDITEM>
                    <![CDATA[0]]>
               </DEPENDITEM>
               <DEPENDVALUE>
                    <![CDATA[]]>
               </DEPENDVALUE>
          </ITEM>";


    return $xml;
}


/**
 * Function to create feedback question XML file to import in feedback object.
 * @param $cours
 * @param $feedback the feedback object with its questions
 */
function create_feedback_questions_xml($cours, $feedback){
    echo "****** Create the Feedback questions bank file \n";

    if(!empty($feedback->questions)){
        //do not create feedbak questions file if no questions in this feedback in Chamilo.

        $fbk_title = strip_tags($feedback->title);
        $fbk_name = preg_replace('/\s+/', '', $fbk_title);
        $fbk_name = substr($fbk_name, 0, 12);

        $fileLocation = "/srv/moodlenfs/importchamilo/qBanks/".$cours->directory."/";
        $qFbkFile = fopen($fileLocation.$cours->code."_feedbackQuestions_".$fbk_name.".xml", "w");

        //Start of file
        $txt_bof = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<FEEDBACK VERSION=\"200701\" COMMENT=\"XML-Importfile for mod/feedback\">
    <ITEMS>
                        ";
        fwrite($qFbkFile, $txt_bof);

        //treat each glossary entry
        foreach($feedback->questions as $q){
            $question_xml = set_feedback_question_xml($q);
            fwrite($qFbkFile, $question_xml);
        }

        //end of file
        $txt_eof = "</ITEMS>
</FEEDBACK>\n";
        fwrite($qFbkFile, $txt_eof);
        fclose($qFbkFile);
    }
}



/**
 * (Not needed anymore) create moodle xml file for glossary export
 * @param $cours
 * @param $gloss_entries
 */
function create_glossary_xml($cours, $gloss_entries){
    echo "****** Create the Glossary entries bank file \n";

    if(!empty($gloss_entries)){
        //do not create Glossb_ank file if no question in Chamilo.

        $fileLocation = "/srv/moodlenfs/importchamilo/qBanks/".$cours->directory."/";
        $qGlossFile = fopen($fileLocation.$cours->code."_GlossaryEntriesBank.xml", "w");

        //Start of file
        $txt_bof = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                <GLOSSARY>
                    <INFO>
                        <NAME>Glossaire</NAME>
                        <INTRO></INTRO>
                        <INTROFORMAT>1</INTROFORMAT>
                        <ALLOWDUPLICATEDENTRIES>0</ALLOWDUPLICATEDENTRIES>
                        <DISPLAYFORMAT>dictionary</DISPLAYFORMAT>
                        <SHOWSPECIAL>1</SHOWSPECIAL>
                        <SHOWALPHABET>1</SHOWALPHABET>
                        <SHOWALL>1</SHOWALL>
                        <ALLOWCOMMENTS>0</ALLOWCOMMENTS>
                        <USEDYNALINK>0</USEDYNALINK>
                        <DEFAULTAPPROVAL>1</DEFAULTAPPROVAL>
                        <GLOBALGLOSSARY>0</GLOBALGLOSSARY>
                        <ENTBYPAGE>10</ENTBYPAGE>
                        <ENTRIES>
                        ";
        fwrite($qGlossFile, $txt_bof);

        //treat each glossary entry
        foreach($gloss_entries as $gloss){
            $entry_xml = "<ENTRY>
                            <CONCEPT>".$gloss->name."</CONCEPT>
                            <DEFINITION>".$gloss->description."</DEFINITION>
                            <FORMAT>1</FORMAT>
                            <USEDYNALINK>0</USEDYNALINK>
                            <CASESENSITIVE>0</CASESENSITIVE>
                            <FULLMATCH>0</FULLMATCH>
                            <TEACHERENTRY>1</TEACHERENTRY>
                          </ENTRY>
                          ";

            fwrite($qGlossFile, $entry_xml);
        }

        //end of file
        $txt_eof = "</ENTRIES>
               </INFO>
            </GLOSSARY>\n";
        fwrite($qGlossFile, $txt_eof);
        fclose($qGlossFile);
    }
}


/**
 * Matches Chamilo categories to Moodle categories
 * @param $chamilo_category_id
 * @return int category id in moodle
 */
function mdl_matching_category($chamilo_category_id){
    //set up default catgeory
    $mdl_category_id = 410;

    //moodle id 0 --> archives
    $array_matching_categories = array();

    $droit_categories = array(
        array(
            'moodle_id' => 62,
            'chamilo_id' => 1,
            'fac' => 'DROIT'
        ),
        array(
            'moodle_id' => 251,
            'chamilo_id' => 229,
            'fac' => 'DROIT'
        )
    );

    $fapse_categories = array(
        array(
            'moodle_id' => 67,
            'chamilo_id' => 4,
            'fac' => 'FPSE',
        ),
        array(
            'moodle_id' => 70,
            'chamilo_id' => 127,
            'fac' => 'FPSE',
        )
    );

    $other_categories = array(
        array(
            'moodle_id' => 78,
            'chamilo_id' => 245,
            'fac' => 'IUFE',
        ),
        array(
            'moodle_id' => 82,
            'chamilo_id' => 39,
            'fac' => 'AUT',
        )
    );

    

    $array_matching_categories = array_merge($droit_categories, $fapse_categories, $other_categories);

    $key = array_search($chamilo_category_id, array_column($array_matching_categories, 'chamilo_id'));
    // $mdl_category_id = $array_matching_categories[$key]['moodle_id'];
    $mdl_category_id = ($key == null) ? $mdl_category_id : $array_matching_categories[$key]['moodle_id'];

    return $mdl_category_id;
}
